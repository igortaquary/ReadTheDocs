# Manuais do Sistema
---

- ## Gestão Contratual
    - ### [Contratos](manuais/gestaoContratual/contratos/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/0604a3feac9d62247bbea383a08192d3/111_GestaoContratualContratos.pdf)

        - Itens Contrato:
            - #### [Arquivo](manuais/gestaoContratual/itensContrato/arquivo/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/72f40713ad275ce32657e99ef7759bdb/112_GestaoContratualContratosItensContratoArquivo.pdf)

            - #### [Cronograma](manuais/gestaoContratual/itensContrato/cronograma/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/3aeb60612cd4f0890289695130e9bd46/113_GestaoContratualContratosItensContratoCronograma.pdf)

            - #### [Despesas acessórias](manuais/gestaoContratual/itensContrato/despesasAcessorias/index.md)

            - #### [Empenhos](manuais/gestaoContratual/itensContrato/empenhos/index.md)

            - #### [Garantias](manuais/gestaoContratual/itensContrato/garantias/index.md)

            - #### [Histórico](manuais/gestaoContratual/itensContrato/historico/index.md)

            - #### [Itens](manuais/gestaoContratual/itensContrato/itens/index.md)

            - #### [Padrões DH SIAFI](manuais/gestaoContratual/itensContrato/padroesDHSIAFI/index.md)

            - #### [Prepostos](manuais/gestaoContratual/itensContrato/prepostos/index.md)

            - #### [Responsáveis](manuais/gestaoContratual/itensContrato/responsaveis/index.md)

        - Modificar Contrato:
            - #### [Instrumento Inicial](manuais/gestaoContratual/modificarContrato/instrumentoInicial/index.md)
            - #### [Termo Aditivo](manuais/gestaoContratual/modificarContrato/termoAditivo/index.md)
            - #### [Termo Apostilamento](manuais/gestaoContratual/modificarContrato/termoApostilamento/index.md)
            - #### [Termo Rescisão](manuais/gestaoContratual/modificarContrato/termoRescisao/index.md)

    - ### Inclusão de Termo Aditivo
        - #### [Supressão/Acréscimo](./manuais/gestaoContratual/inclusaoTermoAditivo/acrescimo/index.md)

        - #### [Prorrogação de Vigência](./manuais/gestaoContratual/inclusaoTermoAditivo/prorrogacao/index.md)

    - ### [Fornecedores](manuais/gestaoContratual/fornecedores/index.md)

    - ### [Indicadores](manuais/gestaoContratual/indicadores/index.md)

    - ### [Sub-Rogações](manuais/gestaoContratual/subRogacoes/index.md)

    - ### Importação SIASG
        - #### [Compras](./manuais/gestaoContratual/importacaoSIASG/compras/index.md)

        - #### [Contratos](./manuais/gestaoContratual/importacaoSIASG/contratos/index.md)

    - ### Consultas:
        - #### [Arquivos](./manuais/gestaoContratual/consultas/arquivos/index.md)

        - #### [Cronogramas](./manuais/gestaoContratual/consultas/cronogramas/index.md)

        - #### [Despesas Acessórias](./manuais/gestaoContratual/consultas/despesasAcessorias/index.md)
        
        - #### [Empenho](./manuais/gestaoContratual/consultas/empenho/index.md)
        
        - #### [Faturas](./manuais/gestaoContratual/consultas/faturas/index.md)

        - #### [Garantias](./manuais/gestaoContratual/consultas/garantias/index.md)

        - #### [Históricos](./manuais/gestaoContratual/consultas/historicos/index.md)

        - #### [Itens](./manuais/gestaoContratual/consultas/itens/index.md)

        - #### [Ocorrências](./manuais/gestaoContratual/consultas/ocorrencias/index.md)

        - #### [Responsáveis](./manuais/gestaoContratual/consultas/responsaveis/index.md)

    - ### Relatórios
        - #### [Contratos da UG](./manuais/gestaoContratual/relatorios/contratosDaUG/index.md)
        - #### [Contratos do Órgão](./manuais/gestaoContratual/relatorios/contratosDoOrgao/index.md)
        - #### [Todos Contratos](./manuais/gestaoContratual/relatorios/todosContratos/index.md)
    
    - ### [Meus Contratos](./manuais/gestaoContratual/meusContratos/index.md)

---

- ## Gestão Financeira
    - ### Apropriação
        - #### [Funcionalidades Apropriação](manuais/gestaoFinanceira/apropriacao/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/b82202f7a7bbe8f79db43da595bc5769/FuncionalidadesApropria%C3%A7%C3%A3o_v510_1509.pdf)


    - ### Cadastro
        - #### [Funcionalidades Empenho](manuais/gestaoFinanceira/empenho/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/5b2d7b37091c97aba4dfe2f643fccbb2/FuncionalidadesEmpenho_v510_1509.pdf)

        - #### [Situação SIAFI](manuais/gestaoFinanceira/situacaoSIAFI/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/f62085451d4a5403fcb718f238301e53/148_GestaoFinanceiraCadastroSituacaoSIAFI.pdf)

        - #### [Rubricas](manuais/gestaoFinanceira/rubricas/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/6faf40f9931c626f2a060dc53e0cbb50/149_GestaoFinanceiraCadastroRubrica.pdf)

        - #### [ RH - Situação](manuais/gestaoFinanceira/RHsituacao/index.md) [![PDF](./manuais/icons/pdf.jpg)](https://gitlab.com/comprasnet/contratos/-/wikis/uploads/0bdf1b6b2c7efad866ac898487e7a124/150_GestaoFinanceiraCadastroRHSituacao.pdf)


---

- ## Administração
    - ### Estrutura
        - #### [Órgão](./manuais/administracao/estrutura/orgao/index.md)

